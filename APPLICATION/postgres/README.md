# PostgreSQL

# Quick reference

- Maintained by: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)
- Where to get help: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)

# Supported tags and respective `Dockerfile` links

- [`10.21-8.6`, `latest`](https://gitee.com/anolis/docker-images/blob/master/postgres/10.21/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/postgres:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a postgres instance

```shell
docker network create some-network
docker run --name some-postgres --network some-network -e POSTGRES_PASSWORD=mysecretpassword -d openanolis/postgres
```

## via `psql`

```shell
docker run -it --rm --network some-network openanolis/postgres psql -h some-postgres -U postgres
```

# PostgreSQL
PostgreSQL, often simply "Postgres", is an object-relational database management system (ORDBMS) with an emphasis on extensibility and standards-compliance.
