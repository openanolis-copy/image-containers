# 龙蜥社区容器镜像仓库

## 仓库说明
龙蜥社区容器镜像仓库，包括 AI 类、基础类、应用类、语言类等。每个应用下都包括一个 version.yml 用于版本控制，buildspec.yml 用于镜像构建
配置。

## version.yml：
主要用于定义AI 框架版本、应用版本 和 操作系统版本、Python 版本、cuda 版本等的依赖关系，构建时根据各版本依赖关系构建出所有镜像。

## buildspec.yml： 
镜像构建相关配置，包括镜像仓库，镜像测试配置，镜像 tag，dockfile 等。

## version.yml、version-base.yml、buildspec.yml 变量定义规则：

### 符号说明：
1. & 表示变量赋值，示例：name: &NAME maven，定义一个 &NAME 变量，值等于 maven
2. *表示变量取值，示例：names: *NAME ，表示取值，等价于 names: maven
3. [] 表示列表, 示例：versions: &VERSIONS [1.3, 4.5] ，定义一个 &VERSIONS数组，值等于  [1.3, 4.5]。数组访问示例：
```
示例1，数组按下标访问，当下标为整数时表示从左往右访问，当下标为负数时表示从右往左访问，当下标超过数组范围时超出部分均取边界值：
   tags: &DATA *VERSIONS[0]  # &DATA = 1.3
   tags: &DATA *VERSIONS[-1]  # &DATA = 4.5
   tags: &DATA *VERSIONS[-2]  # &DATA = 1.3
   tags: &DATA *VERSIONS[-3]  # &DATA = 1.3
   tags: &DATA *VERSIONS[2]  # &DATA = 4.5
示例2，数组支持切片，切片后仍是一个数组，切片规则如下：
   1. a[start:stop]，start, stop 为下标索引，表示一个前闭后开区间，当 start/stop 为正数时表示从左往右索引，当为负数时表示从右往左索引
   tags: &TAG *VERSIONS[0:1]  # &DATA = [1.3]
   tags: &TAG *VERSIONS[0:]  # &DATA = [1.3, 4.5]
   tags: &TAG *VERSIONS[0:-1]  # &DATA = [1.3]
   tags: &TAG *VERSIONS[0:-2]  # &DATA = []
   tags: &TAG *VERSIONS[1:2]  # &DATA = [1.3]
```
4. !join [] 表示字符串拼接, 用于变量赋值。当 [] 中存在数组变量时，字符串会存在多个。使用举例如下：
```
# 示例 1: 定义一个 &TAG 变量，取值为 maven1.21；
tag: &TAG !join [*NAME, "1.21"]
# 示例 2: [] 中只包含一个数组。
# 结果：&TAG 是一个数组，取值为 [maven1.3, maven4.5] 
tag: &TAG !join [*NAME, *VERSIONS]
# 示例 3: [] 中只包含多个数组，拼接顺序为先从最后一个数组进行拆分
# 结果：&TAG 是一个数组，取值为 [maven1.3arg1, maven1.3arg2, maven4.5arg1, maven4.5arg2] 
arg: &ARG [arg1, arg2]
tag: &TAG !join [*NAME, *VERSIONS, *ARG]
```
5. () 定义一个元组，元组表示一组数据，长度固定为 2，且前一个元素值为字符串，后一个元素值为字符串或数组。并支持使用下标方式访问，使用举例如下：
```
示例1，后一个元素为字符串：
   tags: &TAG (test_arg, tag1)
示例2，后一个元素为数组：
   tags: &TAG (test_arg, [tag1, tag2, tag3])
示例3，元组下标访问：
tags: &TAG_DATA *TAG[0]  # &TAG_DATA = test_arg
tags: &TAG_DATA *TAG[1]  # &TAG_DATA = [tag1, tag2, tag3]
```

### 变量引用说明
- 变量需要遵循先定义再使用原则
- 所有变量的作用域在当前构建事务中都是全局的，即在定义处到文件结尾
- 变量允许重新赋值进行覆盖