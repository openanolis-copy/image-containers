# Wrk

# Quick reference

- Maintained by: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)
- Where to get help: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)

# Supported tags and respective `Dockerfile` links

- [`v1.0-8.6`](https://gitee.com/anolis/docker-images/blob/master/wrk_keentune/v1.0/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/wrk_keentune:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a `wrk_keentune` client instance and run stress test

```shell
docker run -it --net=host $IMAGE_ID /bin/bash nginx_http_long.sh $SERVER_IP
docker run -it --net=host $IMAGE_ID /bin/bash nginx_http_short.sh $SERVER_IP
docker run -it --net=host $IMAGE_ID /bin/bash nginx_https_long.sh $SERVER_IP
docker run -it --net=host $IMAGE_ID /bin/bash nginx_https_short.sh $SERVER_IP
```

# Wrk
wrk is a benchmarking tool for the http protocol that uses multithreading and event mode to generate a large amount of load on the target machine 
