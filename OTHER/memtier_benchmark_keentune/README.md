# Memtier_benchmark

# Quick reference

- Maintained by: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)
- Where to get help: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)

# Supported tags and respective `Dockerfile` links

- [`v1.0-8.6`](https://gitee.com/anolis/docker-images/blob/master/memtier_benchmark_keentune/v1.0/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/memtier_benchmark_keentune:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a `memtier_benchmark_keentune` client instance and run stress test
```shell
docker run -it --net=host $IMAGE_ID /bin/bash redis_get.sh $SERVER_IP
docker run -it --net=host $IMAGE_ID /bin/bash redis_set.sh $SERVER_IP
docker run -it --net=host $IMAGE_ID /bin/bash redis_set_get.sh $SERVER_IP
```

# Memtier_benchmark
memtier_benchmark is a command-line tool from Redis Labs that generates a variety of traffic patterns and allows you to benchmark Memcached and Redis instances 
