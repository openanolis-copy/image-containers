# Redis

# Quick reference

- Maintained by: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)
- Where to get help: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)

# Supported tags and respective `Dockerfile` links

- [`v1.0-8.6`](https://gitee.com/anolis/docker-images/blob/master/redis_keentune/v1.0/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/redis_keentune:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a `redis_keentune` server instance and set profile
```shell
docker run -ti -d --privileged="true"  --net=host openanolis/redis_keentune /usr/sbin/init
docker exec -it $CONTAINER_ID /bin/bash redis_keentune.sh
```

# Redis
Redis is an open-source, networked, in-memory, key-value data store with optional durability.
