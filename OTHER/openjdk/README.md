# Openjdk

# Quick reference

- Maintained by: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)
- Where to get help: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)

# Supported tags and respective `Dockerfile` links

- [`17-8.6`, `latest`](https://gitee.com/anolis/docker-images/blob/master/openjdk/17/8.6/Dockerfile)
- [`11-8.6`](https://gitee.com/anolis/docker-images/blob/master/openjdk/11/8.6/Dockerfile)
- [`8-8.6`](https://gitee.com/anolis/docker-images/blob/master/openjdk/8/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/openjdk:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Compile your app inside the Docker container
```shell
docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp "openanolis/openjdk:$VERSION" javac Main.java
```

# Openjdk
OpenJDK (Open Java Development Kit) is a free and open source implementation of the Java Platform, Standard Edition (Java SE). OpenJDK is the official reference implementation of Java SE since version 7.
