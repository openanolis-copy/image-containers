# tensorflow

# Quick reference

- Maintained by: [OpenAnolis AI SIG](https://openanolis.cn/sig/AI_SIG)
- Where to get help: [OpenAnolis AI SIG](https://openanolis.cn/sig/AI_SIG)

# Supported tags and respective `Dockerfile` links

- [`2.12.0-23`](https://gitee.com/anolis/docker-images/blob/master/tensorflow/2.12.0/23/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/tensorflow:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a tensorflow instance
```shell
docker run --name tensorflow-test openanolis/tensorflow:2.12.0-23
```

# Tensorflow
An Open Source Machine Learning Framework for Everyone.
