# 应用版本
ARG DOCKER_VERSION
# BaseOS 版本 [anolisos, 8.8, registry.url]
ARG DOCKER_BASEOS_VERSION
# Python 版本， 3.8/3.9/..
ARG PYTHON_VERSION

# baseos from 地址
ARG OS_FROM=${DOCKER_BASEOS_VERSION[2]}
FROM ${OS_FROM} as base_image

# baseos 名称，anolisos/alinux
ARG OS_TAG = ${DOCKER_BASEOS_VERSION[0]}

RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get autoremove -y \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

ENV PYTHON_PIP_VERSION 8.1.2

############################################################################################################
Install Python
############################################################################################################
RUN set -ex \
        && curl -fSL "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz" -o python.tar.xz \
        && curl -fSL "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz.asc" -o python.tar.xz.asc \
        && export GNUPGHOME="$(mktemp -d)" \
        && gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$GPG_KEY" \
        && gpg --batch --verify python.tar.xz.asc python.tar.xz \
        && rm -r "$GNUPGHOME" python.tar.xz.asc \
        && mkdir -p /usr/src/python \
        && tar -xJC /usr/src/python --strip-components=1 -f python.tar.xz \
        && rm python.tar.xz \
        \
        && cd /usr/src/python \
        && ./configure --enable-shared --enable-unicode=ucs4 \
        && make -j$(nproc) \
        && make install \
        && ldconfig \
        && pip3 install --no-cache-dir --upgrade --ignore-installed pip==$PYTHON_PIP_VERSION \
        && find /usr/local -depth \
                \( \
                    \( -type d -a -name test -o -name tests \) \
                    -o \
                    \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
                \) -exec rm -rf '{}' + \
        && rm -rf /usr/src/python ~/.cache

# make some useful symlinks that are expected to exist
RUN cd /usr/local/bin \
        && ln -s easy_install-3.5 easy_install \
        && ln -s idle3 idle \
        && ln -s pydoc3 pydoc \
        && ln -s python3 python \
        && ln -s python3-config python-config

############################################################################################################
Install Cuda
############################################################################################################

ARG CUDA=${CUDA_VERSION}
ARG CUDA_DASH=11-2
ARG CUDNN=8.2.4.15-1

RUN apt-get update && apt-get install -y --no-install-recommends --allow-unauthenticated --allow-downgrades  --allow-change-held-packages \
   ca-certificates \
   cuda-command-line-tools-${CUDA_DASH} \
   cuda-cudart-dev-${CUDA_DASH} \
   libcufft-dev-${CUDA_DASH} \
   libcurand-dev-${CUDA_DASH} \
   libcusolver-dev-${CUDA_DASH} \
   libcusparse-dev-${CUDA_DASH} \
   curl \
   emacs \
   libcudnn8=${CUDNN}+cuda11.4 \
   libgomp1 \
   libfreetype6-dev \
   libhdf5-serial-dev \
   liblzma-dev \
   libpng-dev \
   libtemplate-perl \
   libzmq3-dev \
   hwloc \
   git \
   unzip \
   wget \
   libtool \
   vim \
   libssl1.1 \
   openssl \
   build-essential \
   openssh-client \
   openssh-server \
   zlib1g-dev \
   # Install dependent library for OpenCV
   libgtk2.0-dev \
   jq \
 && apt-get update \
 && apt-get install -y --no-install-recommends --allow-unauthenticated --allow-change-held-packages \
   libcublas-dev-${CUDA_DASH} \
   libcublas-${CUDA_DASH} \
   # The 'apt-get install' of nvinfer-runtime-trt-repo-ubuntu1804-5.0.2-ga-cuda10.0
   # adds a new list which contains libnvinfer library, so it needs another
   # 'apt-get update' to retrieve that list before it can actually install the
   # library.
   # We don't install libnvinfer-dev since we don't need to build against TensorRT,
   # and libnvinfer4 doesn't contain libnvinfer.a static library.
   # nvinfer-runtime-trt-repo doesn't have a 1804-cuda10.1 version yet. see:
   # https://developer.download.nvidia.cn/compute/machine-learning/repos/ubuntu1804/x86_64/
 && rm -rf /var/lib/apt/lists/* \
 && mkdir -p /var/run/sshd

############################################################################################################
Install Pytorch
############################################################################################################
RUN dnf install tensorflow${DOCKER_VERSION} -y

CMD ["/bin/bash"]