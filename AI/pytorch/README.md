# pytorch

# Quick reference

- Maintained by: [OpenAnolis AI SIG](https://openanolis.cn/sig/AI_SIG)
- Where to get help: [OpenAnolis AI SIG](https://openanolis.cn/sig/AI_SIG)

# Supported tags and respective `Dockerfile` links

- [`2.0.1-23`](https://gitee.com/anolis/docker-images/blob/master/pytorch/2.0.1/23/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/pytorch:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a pytorch instance
```shell
docker run --name pytorch-test openanolis/pytorch:2.0.1-23
```

# Pytorch
Tensors and Dynamic neural networks in Python with strong GPU acceleration.
